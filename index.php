<?php
require_once 'autoload.php';


?>
<!doctype html>
<html lang="nl">

<head>
    <meta charset="utf-8">
    <title>Wobsview</title>
    <meta name="description" content="Wobsview the #1 movie review website">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">

    <link rel="apple-touch-icon" href="icon.png">
    <!-- Als we een icoontje willen toevoegen-->

    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css">
    <script src="http://code.jquery.com/jquery-1.8.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.slider').slick({
                infinite: true,
                slidesToShow: 3,
                rows: 1,
                arrows: true,
                slidesToScroll: 1,
                draggable: true,
            });
        });
    </script>
</head>

<body>

<div id="container">
    <div id="nav">
        <div><img src="https://via.placeholder.com/150" alt="picture"></div>
        <a href="/">Wobsview</a>
        <ul>
            <li><a href="#">Navigatie 1</a></li>
            <li><a href="<?= Application::useUrl() ?>">Navigatie 2</a></li>
            <li><a href="<?= Application::useUrl('views/test.php') ?>">Navigatie 3</a></li>
            <li><a href="#">Navigatie 4</a></li>
        </ul>
    </div>
    <div id="content">
        <div class="latestreview">
            <div class="starrating">Star rating</div>
            <div class="title">Title of latest review</div>
        </div>
    </div>
    <div id="footer">
        <span class="footer">Wobsview copyright 2020</span>
    </div>
</div>
</body>

</html>