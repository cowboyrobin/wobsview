<?php

/**
 * @param string $folder The directory within the lib directory.
 */
function include_php_file($folder)
{
    $folder = __DIR__ . DIRECTORY_SEPARATOR . $folder;

    foreach (glob("{$folder}" . DIRECTORY_SEPARATOR . "*.php") as $filename) {
        include_once $filename;
    }
}

include_php_file('lib');
require_once 'functions.php';

?>



