<form method="post" action="../lib/database/InsertPicture.php">
    <label for="title">Title:</label><br>
    <input type="text" id="title" name="title"><br>
    <label for="rating">Rating:</label>
    <select name="rating" id="rating">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
    </select>
    <br>
    <br>
    <label for="type">Type:</label>
    <select name="type" id="type">
        <option value="movie">Movie</option>
        <option value="show">Show</option>
        <option value="game">Game</option>
    </select>
    <br>
    <br>
    <label for="review:">Review</label>
    <br>
    <textarea id="review" name="review" rows="4" cols="50"></textarea>
    <br>
    <br>
    <input type="submit" value="create">
</form>
