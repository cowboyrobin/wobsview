<?php

spl_autoload_register(function ($classname) {

    $classesPath = __DIR__ . '/classes/';
    $classes = $classesPath . $classname . '.php';
    $databasePath = __DIR__ . '/database/';
    $database = $databasePath . $classname . '.php';

    if (file_exists($classes)) {
        require_once $classes;
    }

});