-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema wobsview_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema wobsview_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `wobsview_db` DEFAULT CHARACTER SET utf8 ;
USE `wobsview_db` ;

-- -----------------------------------------------------
-- Table `wobsview_db`.`picture`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wobsview_db`.`picture` (
  `id` INT(11) NOT NULL,
  `title` VARCHAR(100) NULL DEFAULT NULL,
  `rating` INT(2) NULL DEFAULT NULL,
  `type` ENUM('movie', 'show', 'game') NULL DEFAULT NULL,
  `review` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
